
## 0.1.2 [07-13-2022]

* patch/Auth - Auth Changes

See merge request itentialopensource/adapters/telemetry-analytics/adapter-whatsup_gold!1

---

## 0.1.1 [07-12-2022]

* Bug fixes and performance improvements

See commit 89f6c4d

---
