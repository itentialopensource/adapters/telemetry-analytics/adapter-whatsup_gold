## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for WhatsUp Gold. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for WhatsUp Gold.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>

### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the WhatsUp Gold. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">credentialGet(credentialId, view = 'id', callback)</td>
    <td style="padding:15px">Credential_Get</td>
    <td style="padding:15px">{base_path}/{version}/credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialUpdateCredential(credentialId, body, callback)</td>
    <td style="padding:15px">Credential_UpdateCredential</td>
    <td style="padding:15px">{base_path}/{version}/credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialDeleteCredential(credentialId, callback)</td>
    <td style="padding:15px">Credential_DeleteCredential</td>
    <td style="padding:15px">{base_path}/{version}/credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialGetAssignments(credentialId, view = 'id', deviceView = 'id', pageId, limit, callback)</td>
    <td style="padding:15px">Credential_GetAssignments</td>
    <td style="padding:15px">{base_path}/{version}/credentials/{pathv1}/assignments/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialRemoveCredentialFromAllDevice(credentialId, preventOrphanActiveMonitors, callback)</td>
    <td style="padding:15px">Credential_RemoveCredentialFromAllDevice</td>
    <td style="padding:15px">{base_path}/{version}/credentials/{pathv1}/assignments/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialFind(view = 'id', type = 'all', search, pageId, limit, callback)</td>
    <td style="padding:15px">Credential_Find</td>
    <td style="padding:15px">{base_path}/{version}/credentials/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialAddCredential(body, callback)</td>
    <td style="padding:15px">Credential_AddCredential</td>
    <td style="padding:15px">{base_path}/{version}/credentials/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialGetHelper(input, type = 'meraki_organization', callback)</td>
    <td style="padding:15px">Credential_GetHelper</td>
    <td style="padding:15px">{base_path}/{version}/credentials/-/helper?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialFindAssignments(view = 'id', type = 'all', search, deviceView = 'id', pageId, limit, callback)</td>
    <td style="padding:15px">Credential_FindAssignments</td>
    <td style="padding:15px">{base_path}/{version}/credentials/-/assignments/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialGetTemplate(credentialId, key, callback)</td>
    <td style="padding:15px">Credential_GetTemplate</td>
    <td style="padding:15px">{base_path}/{version}/credentials/{pathv1}/config/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialGetTemplate(key, type = 'all', search, callback)</td>
    <td style="padding:15px">Credential_GetTemplate</td>
    <td style="padding:15px">{base_path}/{version}/credentials/-/config/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialApplyCredentialTemplates(key, body, callback)</td>
    <td style="padding:15px">Credential_ApplyCredentialTemplates</td>
    <td style="padding:15px">{base_path}/{version}/credentials/-/config/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGetAttribute(deviceId, attributeId, callback)</td>
    <td style="padding:15px">Device_GetAttribute</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceUpdateAttribute(deviceId, attributeId, name, value, callback)</td>
    <td style="padding:15px">Device_UpdateAttribute</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDeleteAttribute(deviceId, attributeId, callback)</td>
    <td style="padding:15px">Device_DeleteAttribute</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceFindAttributes(deviceId, names, nameContains, valueContains, pageId, limit, callback)</td>
    <td style="padding:15px">Device_FindAttributes</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/attributes/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceAddAttribute(deviceId, name, value, callback)</td>
    <td style="padding:15px">Device_AddAttribute</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/attributes/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDeleteAllAttributesById(deviceId, callback)</td>
    <td style="padding:15px">Device_DeleteAllAttributesById</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/attributes/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDeleteAttributes(deviceId, body, callback)</td>
    <td style="padding:15px">Device_DeleteAttributes</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/attributes/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDeleteAssignedCredentials(deviceId, credentialId, preventOrphanActiveMonitors, callback)</td>
    <td style="padding:15px">Device_DeleteAssignedCredentials</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/credentials/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceCredentials(deviceId, view = 'id', type = 'all', callback)</td>
    <td style="padding:15px">Device_Credentials</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/credentials/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDeviceAssignCredentials(deviceId, body, callback)</td>
    <td style="padding:15px">Device_DeviceAssignCredentials</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/credentials/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDeleteDeviceAssignedCredentials(deviceId, type = 'all', preventOrphanActiveMonitors, callback)</td>
    <td style="padding:15px">Device_DeleteDeviceAssignedCredentials</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/credentials/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGetConfig(deviceId, callback)</td>
    <td style="padding:15px">Device_GetConfig</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGetPollingConfig(deviceId, callback)</td>
    <td style="padding:15px">Device_GetPollingConfig</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/config/polling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceUpdatePollingConfig(deviceId, body, callback)</td>
    <td style="padding:15px">Device_UpdatePollingConfig</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/config/polling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceUpdateMaintenanceById(deviceId, body, callback)</td>
    <td style="padding:15px">Device_UpdateMaintenanceById</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/config/maintenance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceUpdateMaintenanceSchedule(deviceId, body, callback)</td>
    <td style="padding:15px">Device_UpdateMaintenanceSchedule</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/config/maintenance/schedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devicePollNow(deviceId, callback)</td>
    <td style="padding:15px">Device_PollNow</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/poll-now?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceUpdateMaintenanceBatch(body, callback)</td>
    <td style="padding:15px">Device_UpdateMaintenanceBatch</td>
    <td style="padding:15px">{base_path}/{version}/devices/-/config/maintenance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceUpdateMaintenanceBatchSchedule(body, callback)</td>
    <td style="padding:15px">Device_UpdateMaintenanceBatchSchedule</td>
    <td style="padding:15px">{base_path}/{version}/devices/-/config/maintenance/schedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceUpdatePollingConfigBatch(body, callback)</td>
    <td style="padding:15px">Device_UpdatePollingConfigBatch</td>
    <td style="padding:15px">{base_path}/{version}/devices/-/config/polling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceBulkPollNow(body, callback)</td>
    <td style="padding:15px">Device_BulkPollNow</td>
    <td style="padding:15px">{base_path}/{version}/devices/-/poll-now?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGetProperties(deviceId, callback)</td>
    <td style="padding:15px">Device_GetProperties</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceUpdateProperties(deviceId, body, callback)</td>
    <td style="padding:15px">Device_UpdateProperties</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceUpdatePropertiesBatch(body, callback)</td>
    <td style="padding:15px">Device_UpdatePropertiesBatch</td>
    <td style="padding:15px">{base_path}/{version}/devices/-/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDeleteRoleAssignment(deviceId, roleId, callback)</td>
    <td style="padding:15px">Device_DeleteRoleAssignment</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGetRoles(deviceId, kind = 'all', callback)</td>
    <td style="padding:15px">Device_GetRoles</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/roles/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDeviceAssignRole(deviceId, body, callback)</td>
    <td style="padding:15px">Device_DeviceAssignRole</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/roles/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDeleteDeviceAssignedRoles(deviceId, kind = 'all', callback)</td>
    <td style="padding:15px">Device_DeleteDeviceAssignedRoles</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/roles/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDeviceAssignOsRole(deviceId, body, callback)</td>
    <td style="padding:15px">Device_DeviceAssignOsRole</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/roles/os?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDeviceAssignBrandRole(deviceId, body, callback)</td>
    <td style="padding:15px">Device_DeviceAssignBrandRole</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/roles/brand?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGetTemplate(deviceId, options, callback)</td>
    <td style="padding:15px">Device_GetTemplate</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/config/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceApplyTemplates(body, callback)</td>
    <td style="padding:15px">Device_ApplyTemplates</td>
    <td style="padding:15px">{base_path}/{version}/devices/-/config/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGetOverview(deviceId, view = 'id', callback)</td>
    <td style="padding:15px">Device_GetOverview</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDeleteDevice(deviceId, deleteDiscoveredDevices, callback)</td>
    <td style="padding:15px">Device_DeleteDevice</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGetStatus(deviceId, callback)</td>
    <td style="padding:15px">Device_GetStatus</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceBulkDeleteDevice(callback)</td>
    <td style="padding:15px">Device_BulkDeleteDevice</td>
    <td style="padding:15px">{base_path}/{version}/devices/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceBulkOperationDevice(body, callback)</td>
    <td style="padding:15px">Device_BulkOperationDevice</td>
    <td style="padding:15px">{base_path}/{version}/devices/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupGetDeviceGroup(groupId, view = 'summary', callback)</td>
    <td style="padding:15px">DeviceGroup_GetDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupSearchChildren(groupId, search, view = 'summary', returnHierarchy, limit, pageId, callback)</td>
    <td style="padding:15px">DeviceGroup_SearchChildren</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/children?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupAssignedCredentials(groupId, returnHierarchy, search, deviceView = 'id', credentialView = 'id', type = 'all', pageId, limit, callback)</td>
    <td style="padding:15px">DeviceGroup_AssignedCredentials</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices/-/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupSearchDevices(groupId, returnHierarchy, state = 'Unknown', search, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceGroup_SearchDevices</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupDeviceTemplates(groupId, includeHierarchy, search, options, limit, pageId, callback)</td>
    <td style="padding:15px">DeviceGroup_DeviceTemplates</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices/-/config/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupPollNow(groupId, immediateChildren, search, limit, callback)</td>
    <td style="padding:15px">DeviceGroup_PollNow</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/poll-now?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupGetDeviceGroupStatus(groupId, callback)</td>
    <td style="padding:15px">DeviceGroup_GetDeviceGroupStatus</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnCPUutilizationreportforadevicegroup(groupId, returnHierarchy, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', applyThreshold, overThreshold, thresholdValue, businessHoursId, rollupByDevice, pageId, limit, callback)</td>
    <td style="padding:15px">Return CPU utilization report for a device group.</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices/reports/cpu-utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupReportDeviceDiskFreeSpaceReport(groupId, returnHierarchy, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', applyThreshold, overThreshold, thresholdValue, businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceGroupReport_DeviceDiskFreeSpaceReport</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices/reports/disk-free-space?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupReportDeviceDiskUtilizationReport(groupId, returnHierarchy, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', applyThreshold, overThreshold, thresholdValue, businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceGroupReport_DeviceDiskUtilizationReport</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices/reports/disk-utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupReportDeviceInterfaceDiscardsReport(groupId, returnHierarchy, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceGroupReport_DeviceInterfaceDiscardsReport</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices/reports/interface-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupReportDeviceInterfaceErrorsReport(groupId, returnHierarchy, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceGroupReport_DeviceInterfaceErrorsReport</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices/reports/interface-errors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupReportDeviceInterfaceTrafficReport(groupId, returnHierarchy, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceGroupReport_DeviceInterfaceTrafficReport</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices/reports/interface-traffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupReportDeviceInterfaceUtilizationReport(groupId, returnHierarchy, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', applyThreshold, overThreshold, thresholdValue, businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceGroupReport_DeviceInterfaceUtilizationReport</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices/reports/interface-utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupReportDeviceMemoryUtilizationReport(groupId, returnHierarchy, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', applyThreshold, overThreshold, thresholdValue, businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceGroupReport_DeviceMemoryUtilizationReport</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices/reports/memory-utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupReportDevicePingAvailabilityReport(groupId, returnHierarchy, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', availableApplyThreshold, availableOverThreshold, percentageAvailableThresholdValue, packetLossApplyThreshold, packetLossOverThreshold, packetLossPercentageThresholdValue, businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceGroupReport_DevicePingAvailabilityReport</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices/reports/ping-availability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupReportDevicePingResponseTimeReport(groupId, returnHierarchy, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', applyThreshold, overThreshold, thresholdValue, businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceGroupReport_DevicePingResponseTimeReport</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices/reports/ping-response-time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupReportDeviceStateChangeReport(groupId, returnHierarchy, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', pageId, limit, callback)</td>
    <td style="padding:15px">DeviceGroupReport_DeviceStateChangeReport</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices/reports/state-change?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceGroupReportDeviceMaintenanceMode(groupId, returnHierarchy, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', pageId, limit, callback)</td>
    <td style="padding:15px">DeviceGroupReport_DeviceMaintenanceMode</td>
    <td style="padding:15px">{base_path}/{version}/device-groups/{pathv1}/devices/reports/device-maintenance-mode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceReportDeviceCpuUtilizationReport(deviceId, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', applyThreshold, overThreshold, thresholdValue, businessHoursId, rollupByDevice, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceReport_DeviceCpuUtilizationReport</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/reports/cpu-utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceReportDeviceDiskFreeSpaceReport(deviceId, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', applyThreshold, overThreshold, thresholdValue, businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceReport_DeviceDiskFreeSpaceReport</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/reports/disk-free-space?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceReportDeviceDiskUtilizationReport(deviceId, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', applyThreshold, overThreshold, thresholdValue, businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceReport_DeviceDiskUtilizationReport</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/reports/disk-utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceReportDeviceInterfaceDiscardsReport(deviceId, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceReport_DeviceInterfaceDiscardsReport</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/reports/interface-discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceReportDeviceInterfaceErrorsReport(deviceId, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceReport_DeviceInterfaceErrorsReport</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/reports/interface-errors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceReportDeviceInterfaceTrafficReport(deviceId, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceReport_DeviceInterfaceTrafficReport</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/reports/interface-traffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceReportDeviceInterfaceUtilizationReport(deviceId, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', applyThreshold, overThreshold, thresholdValue, businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceReport_DeviceInterfaceUtilizationReport</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/reports/interface-utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceReportDeviceMemoryUtilizationReport(deviceId, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', applyThreshold, overThreshold, thresholdValue, businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceReport_DeviceMemoryUtilizationReport</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/reports/memory-utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceReportDevicePingAvailabilityReport(deviceId, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', availableApplyThreshold, availableOverThreshold, percentageAvailableThresholdValue, packetLossApplyThreshold, packetLossOverThreshold, packetLossPercentageThresholdValue, businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceReport_DevicePingAvailabilityReport</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/reports/ping-availability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceReportDevicePingResponseTimeReport(deviceId, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', applyThreshold, overThreshold, thresholdValue, businessHoursId, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceReport_DevicePingResponseTimeReport</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/reports/ping-response-time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceReportDeviceStateChangeReport(deviceId, range = 'today', rangeStartUtc, rangeEndUtc, rangeN, sortBy = 'defaultColumn', sortByDir = 'asc', groupBy = 'noGrouping', groupByDir = 'asc', pageId, limit, callback)</td>
    <td style="padding:15px">DeviceReport_DeviceStateChangeReport</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/reports/state-change?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceRoleGetSummaryTemplateById(roleId, view = 'id', callback)</td>
    <td style="padding:15px">DeviceRole_GetSummaryTemplateById</td>
    <td style="padding:15px">{base_path}/{version}/device-role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceRoleRemoveRoleById(roleId, callback)</td>
    <td style="padding:15px">DeviceRole_RemoveRoleById</td>
    <td style="padding:15px">{base_path}/{version}/device-role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returntemplateforadevicerole(roleId, options = 'all', callback)</td>
    <td style="padding:15px">Return template for a device role</td>
    <td style="padding:15px">{base_path}/{version}/device-role/{pathv1}/config/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceRoleEnableRoleById(roleId, callback)</td>
    <td style="padding:15px">DeviceRole_EnableRoleById</td>
    <td style="padding:15px">{base_path}/{version}/device-role/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceRoleDisableRoleById(roleId, callback)</td>
    <td style="padding:15px">DeviceRole_DisableRoleById</td>
    <td style="padding:15px">{base_path}/{version}/device-role/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceRoleRestoreSystemDefaultsById(roleId, callback)</td>
    <td style="padding:15px">DeviceRole_RestoreSystemDefaultsById</td>
    <td style="padding:15px">{base_path}/{version}/device-role/{pathv1}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceRoleGetSummaryList(view = 'id', kind = 'any', source = 'all', name, search, enabledOnly, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceRole_GetSummaryList</td>
    <td style="padding:15px">{base_path}/{version}/device-role/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsetofdeviceroletemplates(options = 'all', kind = 'any', source = 'all', name, search, enabledOnly, pageId, limit, callback)</td>
    <td style="padding:15px">Return set of device role templates</td>
    <td style="padding:15px">{base_path}/{version}/device-role/-/config/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceRoleApplyTemplates(body, callback)</td>
    <td style="padding:15px">DeviceRole_ApplyTemplates</td>
    <td style="padding:15px">{base_path}/{version}/device-role/-/config/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceRoleGetPercentVariables(choice = 'discoveryDevice', callback)</td>
    <td style="padding:15px">DeviceRole_GetPercentVariables</td>
    <td style="padding:15px">{base_path}/{version}/device-role/-/percentVariables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceRoleRoleAssignmentsById(roleId, filter, deviceView = 'id', deviceFilter, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceRole_RoleAssignmentsById</td>
    <td style="padding:15px">{base_path}/{version}/device-role/{pathv1}/assignments/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceRoleAllRoleAssignments(kind = 'all', filter, includeUnassignedRoles, deviceView = 'id', deviceFilter, pageId, limit, callback)</td>
    <td style="padding:15px">DeviceRole_AllRoleAssignments</td>
    <td style="padding:15px">{base_path}/{version}/device-role/-/assignments/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">errorInduceError(errorType, callback)</td>
    <td style="padding:15px">Error_InduceError</td>
    <td style="padding:15px">{base_path}/{version}/errors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitorGetById(monitorId, type = 'active', callback)</td>
    <td style="padding:15px">Monitor_GetById</td>
    <td style="padding:15px">{base_path}/{version}/monitors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitorGetAll(type = 'all', allMonitors, search, callback)</td>
    <td style="padding:15px">Monitor_GetAll</td>
    <td style="padding:15px">{base_path}/{version}/monitors/-?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitorGetTemplateById(monitorId, key, type = 'active', callback)</td>
    <td style="padding:15px">Monitor_GetTemplateById</td>
    <td style="padding:15px">{base_path}/{version}/monitors/{pathv1}/config/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitorGetTemplates(key, type = 'all', allMonitors, search, callback)</td>
    <td style="padding:15px">Monitor_GetTemplates</td>
    <td style="padding:15px">{base_path}/{version}/monitors/-/config/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitorApplyMonitorTemplates(key, body, callback)</td>
    <td style="padding:15px">Monitor_ApplyMonitorTemplates</td>
    <td style="padding:15px">{base_path}/{version}/monitors/-/config/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">productApiVersion(callback)</td>
    <td style="padding:15px">Product_ApiVersion</td>
    <td style="padding:15px">{base_path}/{version}/product/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">productProductVersion(callback)</td>
    <td style="padding:15px">Product_ProductVersion</td>
    <td style="padding:15px">{base_path}/{version}/product/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">productWhoAmI(callback)</td>
    <td style="padding:15px">Product_WhoAmI</td>
    <td style="padding:15px">{base_path}/{version}/product/whoAmI?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
