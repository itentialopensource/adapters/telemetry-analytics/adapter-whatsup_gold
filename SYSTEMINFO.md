# WhatsUp Gold

Vendor: Progress
Homepage: https://www.progress.com/

Product: WhatsUp Gold
Product Page: https://www.whatsupgold.com/

## Introduction
We classify WhatsUp Gold into the Service Assurance domain as WhatsUp Gold provides capabilities in network monitoring and management. 

## Why integrate
The WhatsUp Gold adapter from Itential is used to integrate the Itential Automation Platform (IAP) with WhatsUp Gold to help with cloud based monitoring of network availability and performance monitoring.

With this adapter you have the ability to perform operations with WhatsUp Gold such as:

- Product
- Device
- Monitor
- Manage device monitoring
- Get complete report data for given device or device group

## Additional Product Documentation
The [API documents for WhatsUp Gold](https://docs.ipswitch.com/NM/WhatsUpGold2019_2/02_Guides/rest_api/)