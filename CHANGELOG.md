
## 0.4.5 [10-15-2024]

* Changes made at 2024.10.14_20:00PM

See merge request itentialopensource/adapters/adapter-whatsup_gold!17

---

## 0.4.4 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-whatsup_gold!15

---

## 0.4.3 [08-14-2024]

* Changes made at 2024.08.14_18:08PM

See merge request itentialopensource/adapters/adapter-whatsup_gold!14

---

## 0.4.2 [08-08-2024]

* add authmethod

See merge request itentialopensource/adapters/adapter-whatsup_gold!13

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:21PM

See merge request itentialopensource/adapters/adapter-whatsup_gold!12

---

## 0.4.0 [05-20-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-whatsup_gold!11

---

## 0.3.7 [03-27-2024]

* Changes made at 2024.03.27_13:11PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-whatsup_gold!10

---

## 0.3.6 [03-21-2024]

* Changes made at 2024.03.21_14:56PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-whatsup_gold!9

---

## 0.3.5 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/telemetry-analytics/adapter-whatsup_gold!8

---

## 0.3.4 [03-12-2024]

* Changes made at 2024.03.12_10:57AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-whatsup_gold!7

---

## 0.3.3 [02-27-2024]

* Changes made at 2024.02.27_11:31AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-whatsup_gold!6

---

## 0.3.2 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/telemetry-analytics/adapter-whatsup_gold!5

---

## 0.3.1 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/telemetry-analytics/adapter-whatsup_gold!4

---

## 0.3.0 [11-07-2023]

* More migration changes

See merge request itentialopensource/adapters/telemetry-analytics/adapter-whatsup_gold!3

---

## 0.2.0 [10-02-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-whatsup_gold!2

---

## 0.1.2 [07-13-2022]

* patch/Auth - Auth Changes

See merge request itentialopensource/adapters/telemetry-analytics/adapter-whatsup_gold!1

---

## 0.1.1 [07-12-2022]

* Bug fixes and performance improvements

See commit 89f6c4d

---
